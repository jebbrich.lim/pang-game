using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public abstract class Projectile : MonoBehaviour, IPoolable
{
    public UnityEvent EvtHit { get; } = new();

    [Header("Configuration")]
    public string Id = null;
    public string Name = null;
    public int Damage = 10;
    public float MoveSpeed = 10.0f;
    public float Range = 10.0f;

    public Vector3 StartPosition { get; protected set; }
    public float Distance => Vector3.Distance(transform.position, StartPosition);
    public Team Team { get; private set; }

    protected Rigidbody2D rigidbody2d;
    private bool destroying = false;
    private bool hasStarted = false;

    protected abstract void Move(float deltaTime);
    protected virtual void OnDestroying() { }

    private void InitializeProjectile()
    {
        Team = GetComponent<Team>();
        rigidbody2d = GetComponent<Rigidbody2D>();

        StartPosition = transform.position;
    }

    private void OnHit(Unit unit)
    {
        unit.Health.TakeDamage(Damage);
        EvtHit.Invoke();
    }

    private void DestroyProjectile()
    {
        destroying = true;
        this.Pool();
    }

    private void FixedUpdate()
    {
        if (destroying) return;

        // First frame is for intializing
        if (!hasStarted)
        {
            hasStarted = true;
            InitializeProjectile();
            return;
        }

        // Destroy when projectile is out of range
        if (Distance > Range && Range > 0)
        {
            DestroyProjectile();
        }

        Move(Time.deltaTime);
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    Debug.Log("Hit: " + other.name);
    //    if (destroying) return;
    //    if (!other.attachedRigidbody) return;
    //    if (!other.GetComponent<Team>().IsEnemy(this.Team)) return;

    //    OnHit(other.GetComponent<Unit>());
    //    OnDestroying();
    //    DestroyProjectile();
    //}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (destroying) return;
        if (!other.attachedRigidbody) return;
        if (!other.GetComponent<Team>().IsEnemy(this.Team)) return;

        OnHit(other.GetComponent<Unit>());
        OnDestroying();
        DestroyProjectile();
    }

    void IPoolable.OnPoolAwake()
    {

    }

    void IPoolable.OnPoolSleep()
    {
        destroying = false;
        hasStarted = false;

        StartPosition = Vector3.zero;
    }
}