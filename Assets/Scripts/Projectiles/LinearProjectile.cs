using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearProjectile : Projectile
{
    protected override void Move(float deltaTime)
    {
        Vector3 newPosition = rigidbody2d.position + (MoveSpeed * deltaTime * Vector2.up);
        rigidbody2d.MovePosition(newPosition);
    }
}
