using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StageMode : GameMode
{
    public UnityEvent<int> EvtWaveStarted { get; } = new();
    public UnityEvent<int> EvtWaveEnded { get; } = new();
    public UnityEvent EvtGameStarted { get; } = new();
    public UnityEvent EvtGameLost { get; } = new();

    [Header("Configuration")]
    public int CurrentStage = 1;
    public float LeftBorder = -7.25f;
    public float RightBorder = 7.25f;

    [Header("References")]
    public Player Player;
    public Spawner Spawner;
    public GameObject[] PresetUnitEntities;

    protected override IEnumerator OnLoad()
    {
        Spawner.EvtSpawned.AddListener(OnSpawned);

        EvtGameStarted.Invoke();

        // Initialize Setup
        StartCoroutine(Initialize());

        // Initialize Game Conditions
        StartCoroutine(GameConditions());
        Player.Health.EvtDied.AddListener(OnPlayerDied);

        yield return null;
    }

    private IEnumerator Initialize()
    {
        // Registers already present Units to the spawner
        for (int i = 0; i < PresetUnitEntities.Length; i++)
        {
            Spawner.RegisterUnitEntity(PresetUnitEntities[i]);
            PresetUnitEntities[i].GetComponent<Movement>().Nudge(Vector2.one);
        }

        yield return null;
    }

    private IEnumerator GameConditions()
    {
        yield return null;
    }

    private void OnSpawned(Unit unit)
    {
        //Debug.Log("Spawned: " + unit, unit.gameObject);
    }

    #region Game Conditions
    private void OnPlayerDied(Health health, bool combatDamage)
    {
        Spawner.StopSpawning();
        Spawner.StopAllUnitMovement();
        Lose();
    }

    private void Lose()
    {
        PlayerData playerData = SingletonManager.Get<PlayerData>();
        playerData.HighestScore = Mathf.Max(playerData.HighestScore, playerData.Score);

        SingletonManager.Get<CollectiveUIController>().Show("gameover");
        EvtGameLost.Invoke();
    }
    #endregion

    public void LoadMainMenu()
    {
        GameInstance.LoadGameMode("MainMenu");
    }
}
