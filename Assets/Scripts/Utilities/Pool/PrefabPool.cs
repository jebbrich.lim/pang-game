﻿using System.Collections;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public static class PrefabPool
{
    private static readonly Dictionary<GameObject, List<GameObject>> allPooled = new Dictionary<GameObject, List<GameObject>>();
    private static readonly Dictionary<Scene, Transform> containers = new Dictionary<Scene, Transform>();

    static PrefabPool()
    {
        // Delete pooled objects that was used in the scene
        SceneManager.sceneUnloaded += scene =>
        {
            // Delete pooled
            foreach (List<GameObject> objects in allPooled.Values)
            {
                objects.RemoveAll(o => o == null);
                objects.RemoveAll(o => o.scene == scene);
            }

            // Delete container for scene
            containers.Remove(scene);
        };
    }

    public static GameObject Get(GameObject prefab)
    {
        Assert.IsFalse(prefab.scene.IsValid(), "Must pass in a prefab");

        // Grab list of pooled objects based on prefab
        if (!allPooled.TryGetValue(prefab, out List<GameObject> objects))
        {
            objects = new List<GameObject>();
            allPooled[prefab] = objects;
        }

        // Try to get object from pool
        if (objects.Any())
        {
            // Get from pool
            GameObject pooled = objects[0];
            objects.RemoveAt(0);
            pooled.name = prefab.name;
            pooled.SetActive(true);
            pooled.transform.SetParent(null);
            IPoolable[] poolables = pooled.GetComponentsInChildren<IPoolable>();
            for (int i = 0; i < poolables.Length; i++) poolables[i].OnPoolAwake();

            return pooled;
        }
        else
        {
            // Pool is empty. Instantiate the prefab.
            GameObject obj = Object.Instantiate(prefab);
            obj.name = prefab.name;

            // Attach the poolable
#if UNITY_EDITOR
            if (prefab.GetComponent<Poolable>()) Debug.LogError("Poolable must not be attached manually.", prefab);
#endif
            Poolable poolable = obj.AddComponent<Poolable>();
            poolable.Prefab = prefab;
            IPoolable[] poolables = obj.GetComponentsInChildren<IPoolable>();
            for (int i = 0; i < poolables.Length; i++) poolables[i].OnPoolAwake();

            return obj;
        }
    }

    public static GameObject Get(Component obj)
    {
        return Get(obj.gameObject);
    }

    public static T Get<T>(GameObject prefab)
        where T : Component
    {
        GameObject obj = Get(prefab);
        Assert.IsNotNull(obj.GetComponent<T>(), prefab.name + " doesn't have a " + typeof(T).Name + " component.");

        return obj.GetComponent<T>();
    }

    public static T Get<T>(T obj)
        where T : Component
    {
        return Get<T>(obj.gameObject);
    }

    public static void Pool(GameObject obj)
    {
        // Destroy object if it wasn't fetched from pool
        Poolable poolable = obj.GetComponent<Poolable>();
        if (!poolable)
        {
            Debug.LogWarning(obj.name + " cannot be pooled. It wasn't fetched from the pool. Destroying object...");
            Object.Destroy(obj);
            return;
        }
        if (!poolable.Prefab)
        {
            Debug.LogWarning(obj.name + " wasn't bound to a prefab. Destroying object...");
            Object.Destroy(obj);
            return;
        }

        // Pool
        IPoolable[] poolables = obj.GetComponentsInChildren<IPoolable>();
        for (int i = 0; i < poolables.Length; i++) poolables[i].OnPoolSleep();
        obj.SetActive(false);

        // Add to pool
        allPooled[poolable.Prefab].Add(obj);

        // Add to container
        if (!containers.TryGetValue(obj.scene, out Transform container))
        {
            // Create container if not yet created
            GameObject go = new GameObject("Pooled");
            SceneManager.MoveGameObjectToScene(go, obj.scene);
            container = go.transform;
            containers[obj.scene] = container;
        }
        obj.transform.SetParent(container, false);
    }

    public static bool IsPooled(GameObject obj)
    {
        // Handle prefabs
        if (!obj.scene.IsValid()) return false;

        // Check for poolable component
        Poolable poolable = obj.GetComponent<Poolable>();
        if (!poolable) return false;

        // Find if the object is in the pool
        return allPooled[poolable.Prefab].Contains(obj);
    }
}

public interface IPoolable
{
    void OnPoolAwake();
    void OnPoolSleep();
}
