﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using System.Linq;

public class Poolable : MonoBehaviour {

    private GameObject prefab;
    /// <summary>
    /// Binds this object to a prefab.
    /// Object can only be bound once. If you need to rebind it to a different source, you must create a new instance.
    /// DO NOT INVOKE. This method is invoked internally.
    /// </summary>
    /// <param name="source"></param>
    public GameObject Prefab
    {
        get => prefab;
        set
        {
            Assert.IsNull(prefab, gameObject.name + " is already bound to a prefab.");
            if (!prefab) prefab = value;
        }
    }
}

public static class PoolableExtension
{
    /// <summary>
    /// Pools the GameObject
    /// </summary>
    /// <param name="obj"></param>
    /// <returns>True on successful pool</returns>
    public static void Pool(this GameObject obj)
    {
        PrefabPool.Pool(obj);
    }

    public static void Pool(this Component comp)
    {
        comp.gameObject.Pool();
    }
    
    public static bool IsPooled(this GameObject obj)
    {
        return PrefabPool.IsPooled(obj);
    }

    public static bool IsPooled(this Component comp)
    {
        return IsPooled(comp.gameObject);
    }
}
