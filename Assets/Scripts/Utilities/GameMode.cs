﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/// <summary>
/// GameMode handles the rules and lifecycle of a scene
/// </summary>
public abstract class GameMode : MonoBehaviour 
{
    // List of all additive scenes that are bundled with the GameMode's main scene
    [SerializeField] private string[] additiveScenes = System.Array.Empty<string>();

    public string Id { get { return gameObject.scene.name; } }

    #region Overridables
    // GameMode is still loading
    // Additive scenes are already loaded at this point
    protected virtual IEnumerator OnLoad() { yield break; }
    
    // GameMode is about to unload
    // You can invoke saving of data as the game will wait for this coroutine to finish.
    protected virtual IEnumerator OnUnload() { yield break; }
    #endregion

    #region Static Methods
    public static GameMode Active { get { return GameInstance.ActiveGameMode; } }

    public static T GetActive<T>()
        where T : GameMode
    {
        return GameInstance.GetActiveGameMode<T>();
    }

    public static bool IsGameMode<T>()
        where T : GameMode
    {
        return GameInstance.ActiveGameMode is T;
    }
    #endregion

    private void Start()
    {
        StartCoroutine(LoadTask());
    }

    IEnumerator LoadTask()
    {
        SceneManager.SetActiveScene(gameObject.scene);

        // Load additive scenes
        foreach (string sceneName in additiveScenes)
        {
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        }
        yield return null;

        yield return StartCoroutine(OnLoad());
    }

    public Coroutine Unload()
    {
        return StartCoroutine(UnloadTask());
    }

    IEnumerator UnloadTask()
    {
        yield return StartCoroutine(OnUnload());

        // Unload additive scenes
        foreach (string sceneName in additiveScenes)
        {
            yield return SceneManager.UnloadSceneAsync(sceneName);
        }
    }
}
