﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public static class SingletonManager 
{
	private static Dictionary<System.Type, MonoBehaviour> singletons = new();

    static SingletonManager()
    {
        SceneManager.sceneUnloaded += delegate
        {
            // Remove null singletons
            var toDelete = singletons.Keys.Where(k => !singletons[k]).ToList();
            foreach (var key in toDelete) singletons.Remove(key);
        };
    }

    //Gets a singleton object. Object must be registered.
	public static T Get<T>() where T : MonoBehaviour
	{
        if (singletons.TryGetValue(typeof(T), out MonoBehaviour obj))
        {
            // Clear registered singleton if it was destroyed
            if (!obj) singletons.Remove(typeof(T));
        }

        //Assert.IsNotNull(obj, typeof(T).Name + " singleton doesn't exist");
        return obj as T;
	}

    // Registers the object as a singleton reference.
	public static void Register<T>(T obj) where T : MonoBehaviour
	{
        Assert.IsNull(Get<T>(), typeof(T).Name + " singleton already exists");
        singletons[typeof(T)] = obj;
	}

    // Removes the singleton from the reference list.
    // This will not destroy the object.
	public static void Remove<T>() where T : MonoBehaviour
	{
        singletons.Remove(typeof(T));
	}

    // Removes all singletons from the reference list
	public static void RemoveAll()
	{
		singletons.Clear();
	}
}
