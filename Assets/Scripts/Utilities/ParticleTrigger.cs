using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTrigger : MonoBehaviour
{
    [Header("Configuration")]
    public float OverridePoolDuration = 0.0f;
    public ParticleSystem ParticleSystem;

    public ParticleSystem.MainModule MainModule => ParticleSystem.main;

    public float Duration
    {
        get => MainModule.duration;
        set
        {
            ParticleSystem.MainModule main = MainModule;
            main.duration = value;
        }
    }

    private CountdownTimer cdTimer = null;

    public void Play(bool pool = false)
    {
        ParticleSystem.Play(true);

        if (pool) Pool(Duration);
    }

    public void Play(bool pool, float delay)
    {
        ParticleSystem.Play(true);

        if (pool) Pool(delay);
    }

    public void Stop()
    {
        ParticleSystem.Stop(true);
    }

    public void Clear()
    {
        ParticleSystem.Clear(true);
    }

    public void Pool(float delay)
    {
        if (cdTimer == null)
        {
            cdTimer = new();
            cdTimer.EvtReady.AddListener(OnReady);
        }

        cdTimer.DurationBase = (OverridePoolDuration > 0.0f) ? OverridePoolDuration : delay;
        cdTimer.Start(this);
    }

    private void OnReady(CountdownTimer timer)
    {
        cdTimer.Stop();
        Stop();
        gameObject.Pool();
    }
}
