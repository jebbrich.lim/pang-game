﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine.Assertions;

[System.Serializable]
public class CountdownTimer
{
    public UnityEvent<CountdownTimer> EvtStarted { get; } = new();
    public UnityEvent<CountdownTimer> EvtStopped { get; } = new();
    public UnityEvent<CountdownTimer> EvtReady { get; } = new();

    public float DurationBase = 0;

    public float SpeedModifier { get; set; } = 0;
    public float DurationModifier { get; set; } = 0;
    public bool Started { get; private set; }
    public bool Paused { get; set; }
    public float Duration => DurationBase * (1.0f + DurationModifier);
    public float Remaining { get; set; }
    public bool Ready => Remaining <= 0;
    public float Progress => Ready ? 1.0f : 1.0f - (Remaining / Duration);

    private bool prevReady = false;
    private Coroutine updateHandle = null;
    private MonoBehaviour boundObject = null;

    public void Start(MonoBehaviour boundObject)
    {
        Assert.IsFalse(Started, "Timer already started");
        this.boundObject = boundObject;

        Clear();
        updateHandle = this.boundObject.StartCoroutine(Update());
        Started = true;
        EvtStarted.Invoke(this);
    }

    public void Stop()
    {
        Assert.IsTrue(boundObject, "Cannot stop a timer that is not bound");
        Assert.IsTrue(Started, "Cannot stop a timer that is not started");

        if (updateHandle != null) boundObject.StopCoroutine(updateHandle);
        updateHandle = null;
        Started = false;

        EvtStopped.Invoke(this);
    }

    public void Clear()
    {
        Remaining = Duration;
    }

    public void Refresh()
    {
        Remaining = 0;
    }

    private IEnumerator Update()
    {
        while (true)
        {
            Remaining = Mathf.Max(0, Remaining);

            // Paused
            if (Paused)
            {
                yield return null;
                continue;
            }

            // Timer
            if (Remaining > 0) Remaining -= Time.deltaTime * (1.0f + (float)SpeedModifier);
            else Remaining = 0;

            // Ready check
            if (prevReady != Ready && Ready)
            {
                EvtReady.Invoke(this);
            }
            prevReady = Ready;

            yield return null;
        }
    }
}
    