﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public Team Team { get; private set; }
    public Health Health { get; private set; }

    private void Awake()
    {
        Team = GetComponent<Team>();
        Health = GetComponent<Health>();

        Team.Id = Team.PlayerTeamId;
        Health.Restore();
    }
}
