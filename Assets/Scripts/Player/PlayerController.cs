using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Configuration")]
    public float MoveSpeed = 10.0f;

    public bool IsMoving => moveLeft || moveRight;

    private bool moveLeft = false;
    private bool moveRight = false;

    private PlayerModel model;
    private Rigidbody2D rigidbody2d;
    
    private StageMode stageMode;

    private void Awake()
    {
        model = GetComponent<PlayerModel>();
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        stageMode = GameMode.GetActive<StageMode>();
    }

#if UNITY_EDITOR ||UNITY_STANDALONE_WIN
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            SetMoveLeft(true);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            SetMoveRight(true);
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            SetMoveLeft(false);
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            SetMoveRight(false);
        }
    }
#endif

    private void FixedUpdate()
    {
        if (moveLeft && moveRight)
        {
            return;
        }

        if (moveLeft)
        {
            if (rigidbody2d.position.x > stageMode.LeftBorder)
                rigidbody2d.MovePosition(rigidbody2d.position + (MoveSpeed * Time.deltaTime * Vector2.left));

            model.FlipX = true;
        }

        if (moveRight)
        {
            if (rigidbody2d.position.x <= stageMode.RightBorder) 
                rigidbody2d.MovePosition(rigidbody2d.position + (MoveSpeed * Time.deltaTime * Vector2.right));

            model.FlipX = false;
        }
    }

    public void SetMoveLeft(bool value)
    {
        moveLeft = value;

        SetAnimation();
    }

    public void SetMoveRight(bool value)
    {
        moveRight = value;

        SetAnimation();
    }

    private void SetAnimation()
    {
        model.SetBool("isMoving", (moveLeft || moveRight) && (moveLeft != moveRight));
    }
}
