using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [Header("Configuration")]
    public CountdownTimer Cooldown;

    [Header("References")]
    public GameObject DefaultProjectilePrefab;

    [Header("Effects")]
    public GameObject MuzzleVFX;

    private Team team;
    private PlayerModel model;

    private void Awake()
    {
        team = GetComponent<Team>();
        model = GetComponent<PlayerModel>();
    }

    private void Start()
    {
        Cooldown.Start(this);
    }

    private void OnEnable()
    {
        if (Cooldown.Paused) Cooldown.Paused = false;
    }

    private void OnDisable()
    {
        if (!Cooldown.Paused) Cooldown.Paused = true;
    }

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
    }
#endif

    public void Shoot()
    {
        if (!Cooldown.Ready) return;
        Cooldown.Clear();

        model.SetTrigger("IsAttacking");

        // Muzzle VFX
        if (MuzzleVFX)
        {
            GameObject vfx = PrefabPool.Get(MuzzleVFX);
            vfx.transform.position = model.ProjectileSpawnPoint.position;
            vfx.GetComponent<ParticleTrigger>().Play(true);
        }

        // Create Projectile
        GameObject projectile = PrefabPool.Get(DefaultProjectilePrefab);
        projectile.transform.position = model.ProjectileSpawnPoint.position;
        projectile.transform.rotation = model.ProjectileSpawnPoint.rotation;
        projectile.GetComponent<Team>().Id = this.team.Id;
    }
}
