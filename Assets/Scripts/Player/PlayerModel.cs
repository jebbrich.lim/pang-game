﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.Assertions;

public class PlayerModel : MonoBehaviour
{
    public Transform ProjectileSpawnPoint;

    public Animator Animator { get; private set; }
    public CapsuleCollider2D Collider { get; private set; }
    public bool FlipX
    {
        get => transform.localScale.x < 0;
        set
        {
            if (value == FlipX) return;

            transform.localScale = new Vector3(transform.localScale.x * -1.0f, transform.localScale.y, transform.localScale.z);
        }
    }
    public Color Color { get => spriteRenderer.color; set => spriteRenderer.color = value; }

    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        Animator = GetComponentInChildren<Animator>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    public void SetBool(string id, bool value)
    {
        Animator.SetBool(id, value);
    }

    public void SetTrigger(string id)
    {
        Animator.SetTrigger(id);
    }
}
