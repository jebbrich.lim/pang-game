﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsUIController : MonoBehaviour
{
    [Header("References")]
    public Text ScoreText = null;
    public Text StageText = null;

    private StageMode stageMode;
    private PlayerData playerData;

    private void Start()
    {
        stageMode = GameMode.GetActive<StageMode>();
        playerData = SingletonManager.Get<PlayerData>();
    }

    private void Update()
    {
        ScoreText.text = playerData.Score.ToString("N0");
        StageText.text = stageMode.CurrentStage.ToString("N0");
    }

    public void ExitGame()
    {
        stageMode.LoadMainMenu();
    }
}
