using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.Events;

public class FadeUIAnimation : MonoBehaviour
{
    public UnityEvent<bool> EvtUIAnimationCompleted { get; } = new();

    [System.Serializable]
    public class FadeUIData
    {
        public Graphic Graphic;
        public float Alpha;
        public bool RaycastTarget;

        public FadeUIData(Graphic graphic, float alpha)
        {
            this.Graphic = graphic;
            this.Alpha = alpha;
            this.RaycastTarget = Graphic.raycastTarget;
        }
    }

    [Header("Configuration")]
    [SerializeField] private bool _hideOnStart;
    [SerializeField] private bool _hideRaycast = true;
    // Auto populate if _graphics not filled
    [SerializeField] private Graphic[] _graphics;

    public bool IsShown { get; private set; } = true;

    private List<FadeUIData> fadeData = new();
    private Sequence tweenSequence;

    private void Awake()
    {
        if (_graphics.Length > 0)
        {
            for (int i = 0; i < _graphics.Length; i++)
            {
                fadeData.Add(new FadeUIData(_graphics[i], _graphics[i].color.a));
            }
        }
        else
        {
            Image[] childImages = GetComponentsInChildren<Image>(true);
            TextMeshProUGUI[] childTmproTexts = GetComponentsInChildren<TextMeshProUGUI>(true);
            Text[] childTexts = GetComponentsInChildren<Text>(true);

            for (int i = 0; i < childImages.Length; i++)
            {
                fadeData.Add(new FadeUIData(childImages[i], childImages[i].color.a));
            }

            for (int i = 0; i < childTmproTexts.Length; i++)
            {
                fadeData.Add(new FadeUIData(childTmproTexts[i], childTmproTexts[i].color.a));
            }

            for (int i = 0; i < childTexts.Length; i++)
            {
                fadeData.Add(new FadeUIData(childTexts[i], childTexts[i].color.a));
            }
        }
    }

    private void Start()
    {
        if (_hideOnStart) Hide(0.0f);
    }

    public void Show(float duration = 0.125f)
    {
        if (IsShown) return;
        IsShown = true;

        if (tweenSequence != null) Stop();

        tweenSequence = DOTween.Sequence();
        tweenSequence.SetUpdate(true);

        foreach (FadeUIData data in fadeData)
        {
            tweenSequence.Insert(0.0f, data.Graphic.DOFade(data.Alpha, duration));
        }

        tweenSequence.OnComplete(() => OnComplete(true));
        tweenSequence.Play();
    }

    public void Hide(float duration = 0.125f)
    {
        if (!IsShown) return;
        IsShown = false;

        if (tweenSequence != null) Stop();

        if (duration <= 0.0f)
        {
            foreach (FadeUIData data in fadeData)
            {
                Color graphicColor = data.Graphic.color;
                graphicColor.a = 0.0f;
                data.Graphic.color = graphicColor;
                data.Graphic.raycastTarget = false;
            }

            return;
        }

        tweenSequence = DOTween.Sequence();
        tweenSequence.SetUpdate(true);

        foreach (FadeUIData data in fadeData)
        {
            tweenSequence.Insert(0.0f, data.Graphic.DOFade(0.0f, duration));
        }

        tweenSequence.OnComplete(() => OnComplete(false));
        tweenSequence.Play();
    }

    public void Stop()
    {
        if (tweenSequence != null)
        {
            if (tweenSequence.IsPlaying()) tweenSequence.Kill(true);
            tweenSequence = null;
        }
    }

    private void OnComplete(bool show)
    {
        if (tweenSequence != null)
        {
            if (tweenSequence.IsPlaying()) tweenSequence.Kill();
            tweenSequence = null;
        }

        if (_hideRaycast)
        {
            foreach (FadeUIData data in fadeData)
            {
                data.Graphic.raycastTarget = (show) ? data.RaycastTarget : false;
            } 
        }

        EvtUIAnimationCompleted.Invoke(show);
    }
}
