using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHitThresholdUIController : MonoBehaviour
{
    [Header("Configuration")]
    [SerializeField] [Range(0.0f, 1.0f)] private float _alphaHitThreshold = 0.5f;

    private void Awake()
    {
        GetComponent<Image>().alphaHitTestMinimumThreshold = _alphaHitThreshold;
    }
}
