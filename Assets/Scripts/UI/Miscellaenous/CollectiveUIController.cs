using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

public class CollectiveUIController : MonoBehaviour
{
    public UnityEvent<string> EvtShown { get; } = new();
    public UnityEvent<string> EvtHidden { get; } = new();

    [System.Serializable]
    public struct InterfaceData
    {
        public string Id;
        public GameObject Interface;
        public bool ShowAwake;
        public bool ExemptFromHiding;
    }

    [Header("References")]
    [SerializeField] private InterfaceData[] _interfaces;

    private void Awake()
    {
        SingletonManager.Register(this);
    }

    private void Start()
    {
        for (int i = 0; i < _interfaces.Length; i++)
        {
            if (_interfaces[i].ShowAwake)
            {
                _interfaces[i].Interface.SetActive(true);
            }
        }
    }

    public GameObject Show(string id, bool hideOthers = false)
    {
        GameObject ui = null;

        for (int i = 0; i < _interfaces.Length; i++)
        {
            if (_interfaces[i].Id == id)
            {
                ui = _interfaces[i].Interface;
                _interfaces[i].Interface.SetActive(true);
                break;
            }
        }

        if (hideOthers)
        {
            for (int i = 0; i < _interfaces.Length; i++)
            {
                if (_interfaces[i].Id != id)
                {
                    if (_interfaces[i].ExemptFromHiding) continue;

                    _interfaces[i].Interface.SetActive(false);
                }
            }
        }

        return ui;
    }

    public void ButtonShow(string id)
    {
        Show(id, true);
    }

    public GameObject Show(string id)
    {
        return Show(id, true);
    }

    public void Hide(string id)
    {
        for (int i = 0; i < _interfaces.Length; i++)
        {
            if (_interfaces[i].Id == id)
            {
                _interfaces[i].Interface.SetActive(false);
                break;
            }
        }
    }

    public void HideAll()
    {
        for (int i = 0; i < _interfaces.Length; i++)
        {
            _interfaces[i].Interface.SetActive(false);
        }
    }

    public void HideAll(bool exempt)
    {
        for (int i = 0; i < _interfaces.Length; i++)
        {
            if (exempt)
                if (_interfaces[i].ExemptFromHiding)
                    continue;

            _interfaces[i].Interface.SetActive(false);
        }
    }
}
