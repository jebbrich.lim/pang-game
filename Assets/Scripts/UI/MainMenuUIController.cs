using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUIController : MonoBehaviour
{
    private MainMenuMode mainMenuMode;

    private void Start()
    {
        mainMenuMode = GameMode.GetActive<MainMenuMode>();
    }

    public void StartGame()
    {
        mainMenuMode.LoadStage();
    }
}