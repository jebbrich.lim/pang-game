using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverUIController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Text _scoreText = null;
    [SerializeField] private Text _highScoreText = null;

    private StageMode stageMode;
    private PlayerData playerData;

    private void Start()
    {
        stageMode = GameMode.GetActive<StageMode>();
        playerData = SingletonManager.Get<PlayerData>();
    }

    private void Update()
    {
        _scoreText.text = playerData.Score.ToString("N0");
        _highScoreText.text = playerData.HighestScore.ToString("N0");
    }

    public void ExitGame()
    {
        stageMode.LoadMainMenu();
    }
}