using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [Header("Configuration")]
    public Vector2 InitialNudge;

    private Rigidbody2D rigidbody2d;

    public void Initialize()
    {
        if (!rigidbody2d) rigidbody2d = GetComponent<Rigidbody2D>();

        rigidbody2d.velocity = Vector2.zero;
    }

    public void Nudge(Vector2 direction)
    {
        rigidbody2d.AddForce(InitialNudge * direction);
    }
}
