using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Spawner : MonoBehaviour
{
    public UnityEvent<Unit> EvtSpawned { get; } = new();

    [Header("Configuration")]
    public string TeamId = "enemy";

    public bool IsSpawning => spawnHandlers.Count > 0;

    private Dictionary<float, Coroutine> spawnHandlers = new();
    private List<Unit> spawned = new();

    public IReadOnlyList<Unit> Spawned => spawned;

    public void Spawn(GameObject prefab, Vector3 origin, int amount)
    {
        Coroutine handler = StartCoroutine(Spawning(spawnHandlers.Count, prefab, origin, amount));
        spawnHandlers.Add(Time.realtimeSinceStartup, handler);
    }

    public void StopSpawning()
    {
        foreach(Coroutine spawning in spawnHandlers.Values)
        {
            StopCoroutine(spawning);
        }

        spawnHandlers.Clear();
    }

    public void RegisterUnitEntity(GameObject gameObject)
    {
        Unit unit = gameObject.GetComponent<Unit>();

        unit.Initialize();
        unit.Health.EvtDied.AddListener(OnUnitDied);

        spawned.Add(unit);
    }

    private IEnumerator Spawning(int id, GameObject prefab, Vector3 origin, int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            Unit newUnit = SpawnUnit(prefab, origin);

            // Specifically added for balloons
            if (i % 2 == 0)
                newUnit.Movement.Nudge(Vector2.right);
            else
                newUnit.Movement.Nudge(Vector2.left);

            yield return null;
        }

        yield return null;
        spawnHandlers.Remove(id);
    }

    private Unit SpawnUnit(GameObject prefab, Vector3 origin)
    {
        Unit newUnit = PrefabPool.Get(prefab).GetComponent<Unit>();
        newUnit.transform.SetParent(this.transform);

        // Setup
        newUnit.Initialize();
        newUnit.Team.Id = TeamId;
        newUnit.Health.EvtDied.AddListener(OnUnitDied);
        newUnit.transform.position = origin;

        spawned.Add(newUnit);
        EvtSpawned.Invoke(newUnit);

        return newUnit;
    }

    private void OnUnitDied(Health health, bool combatDamage)
    {
        health.EvtDied.RemoveListener(OnUnitDied);
        spawned.Remove(health.GetComponent<Unit>());
        health.Pool();
    }

    public void StartAllUnitMovement()
    {
        for (int i = 0; i < spawned.Count; i++)
        {
            spawned[i].Movement.enabled = true;
        }
    }

    public void StopAllUnitMovement()
    {
        for (int i = 0; i < spawned.Count; i++)
        {
            spawned[i].Movement.enabled = false;
        }
    }
}
