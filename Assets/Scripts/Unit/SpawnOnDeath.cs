    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnDeath : MonoBehaviour
{
    [System.Serializable]
    public struct SpawnOnDeathData
    {
        public GameObject Prefab;
        public int Amount;
    }

    [Header("Configuration")]
    public SpawnOnDeathData[] Spawn;

    private void Start()
    {
        if (Spawn.Length <= 0) return;

        GetComponent<Health>().EvtDied.AddListener(OnDied);
    }

    private void OnDied(Health health, bool combat)
    {
        StageMode stageMode = GameMode.GetActive<StageMode>();

        for (int i = 0; i < Spawn.Length; i++)
        {
            SpawnOnDeathData spawnData = Spawn[i];
            stageMode.Spawner.Spawn(spawnData.Prefab, this.transform.position, spawnData.Amount);
        }
    }
}
