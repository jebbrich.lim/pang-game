using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    [Header("Configuration")]
    public string Id;
    public string Name;

    public Team Team { get; private set; }
    public Health Health { get; private set; }
    public Movement Movement { get; private set; }
    public CapsuleCollider2D Collider { get; private set; }

    public void Initialize()
    {
        if (!Team) Team = GetComponent<Team>();
        if (!Health) Health = GetComponent<Health>();
        if (!Movement) Movement = GetComponent<Movement>();
        if (!Collider) Collider = GetComponent<CapsuleCollider2D>();

        Health.Restore();
        Movement.Initialize();
    }

    public Vector3 GetPosition()
    {
        return this.transform.position + new Vector3(Collider.offset.x, Collider.offset.y);
    }
}
